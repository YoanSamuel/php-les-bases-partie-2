<?php

namespace App\Tests\POO\Ex05;

use App\Tests\SimplonTestCase;
use Symfony\Component\Process\Process;

final class NightsWatchTest extends SimplonTestCase
{
    protected const DIR = __DIR__;

    public function testNightsWatchClass(): void
    {
        include_once __DIR__ . '../../../../resources/Tests/testEx05-1.php';

        $this->expectOutputString(
            "* looses his wolf on the enemy, and charges with courage *\n"
                . "* flees, finds a girl, grows a spine, and defends her to the bitter end *\n"
        );
    }

    public function testExtendedIncompleteHouseClass(): void
    {
        $process = new Process(['php', __DIR__ . '/../../../resources/Tests/testEx05-2.php']);
        $process->run();

        self::assertMatchesRegularExpression(
            '/\s*Fatal error:.*1 abstract method and must therefore be declared abstract or implement the remaining methods/',
            $process->getErrorOutput()
        );
    }

    protected static function getExpectedFiles(): array
    {
        return [
            static::getActualFile(true),
            'IFighter.php',
        ];
    }
}
