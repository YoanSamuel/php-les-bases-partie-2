<?php

namespace App\POO\Ex06;

class UnholyFactory
{
    // je créé un tableau pour stocker tous mes soldats
    public $list = [];

    // création de la fonction absorb
    public function absorb($fighter)
    {
        // je vérifie que la classe de mes soldats est enfant a celle de fighter et que la propriété name existe
        if (!is_subclass_of($fighter, 'Fighter') && !property_exists($fighter, 'name')) {
            echo "(Factory can't absorb this, it's not a fighter)\n";

            return $this;
        }
        // je vérifie que je n'ai pas absorbé deux fois le même type de soldat, si oui je l'exprime
        foreach ($this->list as $warrior) {
            if ($warrior->name == $fighter->name) {
                echo '(Factory already absorbed a fighter of type ' . $fighter->name . ")\n";

                return $this;
            }
        }
        // j'implémente mon tableau avec tous mes types de soldat absorbés
        array_push($this->list, $fighter);
        echo '(Factory absorbed a fighter of type ' . $fighter->name . ")\n";

        return $this;
    }

    // création de la fonction fabrication
    public function fabricate($fighter)
    {
        foreach ($this->list as $warrior) {
            if ($warrior->name == $fighter) {
                echo '(Factory fabricates a fighter of type ' . $fighter . ")\n";

                return $warrior;
            }
        }
        echo "(Factory hasn't absorbed any fighter of type " . $fighter . ")\n";

        return null;
    }
}
