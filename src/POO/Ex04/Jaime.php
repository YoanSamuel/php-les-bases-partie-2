<?php

namespace App\POO\Ex04;

use App\Resources\Classes\Lannister\Lannister;

class Jaime extends Lannister
{
    public function sleepWith($class)
    {
        if (is_subclass_of($class, 'App\Resources\Classes\Lannister\Lannister') && is_a($class, 'App\Resources\Classes\Lannister\Cersei')) {
            echo "With pleasure, but only in a tower in Winterfell, then.\n";
        } elseif (is_subclass_of($class, 'App\Resources\Classes\Lannister\Lannister')) {
            echo "Not even if I'm drunk !\n";
        } else {
            echo "Let's do this.\n";
        }
    }
}
