<?php

namespace App\POO\Ex05;

class NightsWatch
{
    public $recruited;

    public function recruit($new)
    {
        $this->recruited[] = $new;
    }

    public function fight()
    {
        foreach ($this->recruited as $new_recruit) {
            if (method_exists($new_recruit, 'fight')) {
                $new_recruit->fight();
            }
        }
    }
}
