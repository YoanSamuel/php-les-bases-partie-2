<?php

namespace App\POO\Ex02;

class Targaryen
{
    public const BURN = 'burns alive';
    public const RESIST = 'emerges naked but unharmed';

    public function getBurned(): string
    {
        if (method_exists($this, 'resistsFire')) {
            return Targaryen::RESIST;
        } else {
            return Targaryen::BURN;
        }
    }
}
