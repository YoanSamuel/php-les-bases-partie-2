<?php

namespace App\POO\Ex03;

abstract class House
{
    abstract public function getHousename();

    abstract public function getHouseSeat();

    abstract public function getHouseMotto();

    public function introduce()
    {
        echo 'House ' . $this->getHouseName() . ' of ' . $this->getHouseSeat() . ' ' . ': "' . $this->getHouseMotto() . '"' . "\n";
    }
}
